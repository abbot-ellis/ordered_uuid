"""Generate secure UUIDs ordered by time created."""
# uuid.py
# Author: Jaron S. C. Powser

# Standard Library Imports
import os
import secrets
import time
import uuid
from datetime import datetime, timezone


# Time offset used by Python uuid library
# See https://github.com/python/cpython/blob/3.8/Lib/uuid.py
#     function uuid1()
_TIME_OFFSET = 0x1b21dd213814000

_MAC_BITS = 48
_MAX_MAC_VALUE = 0xffffffffffff
_MULTICAST_MAC_MASK = 0x10000000000

_CLOCK_SEQ_AND_VARIANT_BITS = 16


class OrderedUUID(uuid.UUID):
    def __init__(self, clock_seq=None, private_mac=True):
        time_and_version = _get_timestamp()

        # OR clock sequence bits to indicate RESERVED_FUTURE variant
        # See https://github.com/python/cpython/blob/3.8/Lib/uuid.py
        #     RESERVED_FUTURE string definition
        if clock_seq is None:
            clock_seq_and_variant = (
                secrets.randbits(_CLOCK_SEQ_AND_VARIANT_BITS) | 0xE000
            )
        else:
            clock_seq_and_variant = clock_seq | 0xE000

        if private_mac:
            node = secrets.randbits(_MAC_BITS) | _MULTICAST_MAC_MASK
        else:
            node = uuid.getnode()

        uuid_int = (time_and_version << 64) + (clock_seq << 48) + node 
        super(OrderedUUID, self).__init__(int=uuid_int)

    # TODO: the next four time functions need review
    @property
    def time_low(self):
        return (self.int >> 68) & 0xffffffff

    @property
    def time_mid(self):
        return (self.int >> 100) & 0xffff

    @property
    def time_hi_version(self):
        version = (self.int >> 64) & 0xf
        time_hi = self.int >> 116
        return (time_hi << 4) + version

    @property
    def time_hi(self):
        return self.int >> 116
    # END TODO

    @property
    def version(self):
        return (self.int >> 64) & 0xf

    @property 
    def asctime_local(self):
        return time.asctime(time.localtime(self.time_seconds))

    @property 
    def asctime_utc(self):
        return time.asctime(time.gmtime(self.time_seconds))

    @property
    def ctime(self):
        return time.ctime(self.time_seconds)

    @property
    def datetime_utc(self):
        """Return a timezone-aware datetime object in UTC."""
        return datetime.fromtimestamp(self.time_seconds, timezone.utc)
        
    @property
    def datetime_local(self):
        """Return a timezone-aware datetime object in local time."""
        return NotImplemented

    @property
    def time_micros(self):
        return NotImplemented

    @property
    def time_ns(self):
        return ((self.int >> 68) - _TIME_OFFSET) * 100

    @property
    def time_precise(self):
        return (self.int >> 68) - _TIME_OFFSET

    @property
    def time_seconds(self):
        return ((self.int >> 68) - _TIME_OFFSET) // 10000000

def _get_timestamp():
    """Generate a timestamp for use with an OrderedUUID."""
    #TODO: Make safe according to Python's UUID library
    timestamp = (time.time_ns() // 100) + _TIME_OFFSET

    # Use max value as version number to indicate nonstandard
    time_and_version_int = (timestamp << 4) + 0xf
    return time_and_version_int

